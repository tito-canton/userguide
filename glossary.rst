Glossary
========

.. glossary::

    BBH
        Binary black hole system, a binary system composed of two black holes.

    BNS
        Binary neutron star, a binary system composed of two neutron stars.

    BNS range
        A figure of merit to describe the sensitivity of a gravitational-wave
        detector to :term:`BNS` mergers, defined as the average luminosity
        distance at which the merger of two :math:`1.4~M_\odot` objects would
        be detectable with a signal to noise ratio of 8. See also :term:`Burst
        range`.

    Burst range
        A figure of merit to describe the sensitivity of a gravitational-wave
        detector to unmodeled bursts, defined with reference to optimistic
        models of gravitational wave emissions from stellar collapse as the
        average luminosity distance at which a monochromatic burst at 150 Hz
        with a fluence of :math:`E_\mathrm{GW} = 10^{-2} M_\odot c^2` would be
        detectable with a signal to noise ratio of 8. See also :term:`BNS
        range`.

    CBC
        Compact binary coalescence.

    FITS
        Flexible Image Transport System, a format for astronomical tables,
        images, and multidimensional data sets.

    GCN
        The Gamma-ray Coordinates Network (https://gcn.gsfc.nasa.gov), a portal
        for discoveries and observations of astronomical transients.
        Historically, GCN has served high-energy satellites but now also other
        electromagnetic wavelengths and also gravitational-wave, cosmic ray,
        and neutrino facilities.

    GCN Circular
        A human-readable astronomical bulletin distributed through :term:`GCN`.

    GCN Notice
        A machine-readable alert distributed through :term:`GCN`.

    GraceDB
        Gravitational Wave Candidate Event Database (https://gracedb.ligo.org),
        the official public marshal portal for LIGO/Virgo candidates.

    GRB
        Gamma-ray burst.

    HEALPix
        Hierarchical Equal Area isoLatitude Pixelation, a scheme for indexing
        positions on the unit sphere.

    HEN
        High Energy Neutrino, particularly in the context of multi-messenger
        GW+HEN follow-up.

    KAGRA
        Kamioka Gravitational Wave Detector, an underground gravitational-wave
        detector in the Kamioka mine in Japan.

    LHO
        LIGO Hanford Observatory (see `LHO observatory home page
        <https://www.ligo.caltech.edu/WA>`_), site of a 4 km gravitational-wave
        detector in Hanford, Washington, USA.

    LLO
        LIGO Livingston Observatory (see `LLO observatory home page
        <https://www.ligo.caltech.edu/LA>`_), site of a 4 km gravitational-wave
        detector in Livingston, Louisiana, USA.

    MassGap
        Compact binary systems with at least one compact object whose mass is
        in the hypothetical "mass gap" between neutron stars and black holes,
        defined here as 3-5 solar masses.

    NSBH
        Neutron star black hole, a binary system composed of one neutron star
        and one black hole.

    primary
        When referring to the two component compact objects or the masses of
        the two component compact objects in a binary, the `primary` is the
        more massive one, i.e., :math:`m_1 \geq m_2`. See :term:`secondary`.

    secondary
        When referring to the two component compact objects or the masses of
        the two component compact objects in a binary, the `secondary` is the
        less massive one, i.e., :math:`m_2 \leq m_1`. See :term:`primary`.

    SN
        Supernova.

    SNR
        Signal-to-noise ratio, here applied to gravitational-wave signals. It
        is defined the square root of the integral over frequency of the power
        spectral density of the gravitational-wave signal over the integral
        over frequency of the average power spectral density of the noise.

    Virgo
        Virgo Observatory (see `Virgo observatory home page
        <http://www.virgo-gw.eu>`_), site of a 3 km gravitational-wave detector
        in Cascina, Italy.

    VOEvent
        An XML format for describing astronomical transients. For the
        specification, see the official `VOEvent IVOA Recommendation
        <http://www.ivoa.net/documents/VOEvent/index.html>`_.

    VTP
        VOEvent Transport Protocol, a simple TCP-based protocol for sending and
        receiving VOEvents, used by :term:`GCN`. For the specification, see the
        official `VTP IVOA recommendation
        <http://www.ivoa.net/documents/Notes/VOEventTransport/>`_.
